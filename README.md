## Установка
Для запуска необходим node.js версии не ниже 7. Ссылка для установки https://nodejs.org/en/.

В папке с проектом выполнить команды:
```
npm i
npm run config
```

### Описание параметров
При запуске `npm run config` скрипт попросит ввести параметры подключения. Вручную их можно будет отредактировать в файле `config/config.json`.
 
- `url` - публичный адрес ARI
- `username` - имя пользователя, который создается в asterisk
- `password` - пароль

Ссылка на описание параметров `app`, `context` и `endpoint`:
https://wiki.asterisk.org/wiki/display/AST/Asterisk+14+Channels+REST+API#Asterisk14ChannelsRESTAPI-originate

## Запуск
```
npm start
```
После запуска надо будет ввести номер, на который будет произведен звонок. Только цифры, без +7 или 8. Например `9991231212`