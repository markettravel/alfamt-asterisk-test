const prompt = require('prompt');
const fs = require('fs');
const path = require('path');

const configPath = path.resolve(__dirname, 'config.json');
const configDefaultPath = path.resolve(__dirname, 'config.default.json');

function readFile(path) {
  return new Promise((res, rej) => {
    fs.readFile(path, 'utf8', (err, data) => {
      if (err) return res({});
      try {
        res(JSON.parse(data));
      } catch (e) {
        res({});
      }
    });
  });
}

Promise.all([
  readFile(configPath),
  readFile(configDefaultPath),
]).then(([config, configDefault]) => {
  const neededParams = configDefault.filter(param => config[param.name] === undefined);

  if (!neededParams.length) {
    console.log('Config done');
    return;
  }

  prompt.message = '';
  prompt.start();
  prompt.get(neededParams, (err, result) => {
    fs.writeFile(
      configPath,
      JSON.stringify(Object.assign(config, result), null, 2),
      () => console.log('Config saved')
    );
  });
});
