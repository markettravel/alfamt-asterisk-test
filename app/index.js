const client = require('ari-client');
const config = require('../config/config.json');
const promisify = require('util').promisify;
const helpers = require('./helpers');
const prompt = require('prompt');

function connect(url, username, password, app) {
  return new Promise((res, rej) => {
    let isConnected = false;
    client.connect(url, username, password, (err, ari) => {
      if (err) return rej(err);
      isConnected = true;
      ari.start(app);
      res(ari);

    });
    setTimeout(() => {
      if (!isConnected) rej('connect_error');
    }, 3000);
  });
}

function playSound(ari, channel) {
  return new Promise((res, rej) => {
    const playback = ari.Playback();

    channel.play({ media: 'sound:hello-world', lang: 'en' }, playback)
      .catch(e => rej(e));

    playback.once('PlaybackFinished', () => {
      res(playback);
    });
  });
}

function getNumber() {
  prompt.start();
  return promisify(prompt.get)(['phone']);
}

(async () => {
  const { phone } = await getNumber();

  const ari = await connect(config.url, config.username, config.password, config.app);
  const channel = ari.Channel();
  const channelId = helpers.normalizeId(channel.id);

  channel.originate({
    app:      config.app,
    context:  config.context,
    endpoint: config.endpoint.replace('${number}', phone),
    timeout:  2,
  });

  ari.on('Dial', async (event) => {
    if (helpers.normalizeId(event.caller.id) !== channelId) return;
    if (event.dialstatus === 'ANSWER') {
      await playSound(ari, channel);
      await channel.hangup();
      process.exit();
    }
  });
})();

