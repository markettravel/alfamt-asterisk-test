module.exports = {
  normalizeId:    id => id.indexOf(';2') !== -1 ? id.substr(0, id.length - 2) : id,
  randomInteger:  (min, max) => Math.round(min - 0.5 + Math.random() * (max - min + 1)),
  isNotAvailable: (pool, phone) => {
    for (let id in pool) {
      const dial = pool[id];
      if (dial.operator.number === phone) return true;
    }
    return false;
  }
};